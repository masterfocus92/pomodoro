﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        int seconds = 0;
        int minutes = 25;

        private void StartButton_Click(object sender, EventArgs e)
        {
            timer1.Enabled = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (seconds == 0)
            {
                seconds = 60;
                minutes -= 1;
            }
            seconds -= 1;
            SecondsLabel.Text = seconds.ToString();
            MinutesLabel.Text = minutes.ToString();

            if (minutes == 0 & seconds == 0)
            {
                TimerStop();
                MarioMusic();
            }
        }

        private void pauseButton_Click(object sender, EventArgs e)
        {
            timer1.Stop();
        }

        private void stopButton_Click(object sender, EventArgs e)
        {
            TimerStop();
        }

        private void TimerStop()
        {
            seconds = 0;
            minutes = 25;
            SecondsLabel.Text = "00";
            MinutesLabel.Text = "25";
            timer1.Stop();
        }

        private void MarioMusic()
        {
            Console.Beep(659, 125);
            Console.Beep(659, 125);
            System.Threading.Thread.Sleep(125);
            Console.Beep(659, 125);
            System.Threading.Thread.Sleep(167);
            Console.Beep(523, 125);
            Console.Beep(659, 125);
            System.Threading.Thread.Sleep(125);
            Console.Beep(784, 125);
            System.Threading.Thread.Sleep(375);
            Console.Beep(392, 125);
            System.Threading.Thread.Sleep(375);
            Console.Beep(523, 125);
            System.Threading.Thread.Sleep(250);
            Console.Beep(392, 125);
            System.Threading.Thread.Sleep(250);
            Console.Beep(330, 125);
            System.Threading.Thread.Sleep(250);
            Console.Beep(440, 125);
            System.Threading.Thread.Sleep(125);
            Console.Beep(494, 125);
            System.Threading.Thread.Sleep(125);
            Console.Beep(466, 125);
            System.Threading.Thread.Sleep(42);
            Console.Beep(440, 125);
            System.Threading.Thread.Sleep(125);
            Console.Beep(392, 125);
            System.Threading.Thread.Sleep(125);
            Console.Beep(659, 125);
            System.Threading.Thread.Sleep(125);
            Console.Beep(784, 125);
            System.Threading.Thread.Sleep(125);
            Console.Beep(880, 125);
            System.Threading.Thread.Sleep(125);
            Console.Beep(698, 125);
            Console.Beep(784, 125);
            System.Threading.Thread.Sleep(125);
            Console.Beep(659, 125);
            System.Threading.Thread.Sleep(125);
            Console.Beep(523, 125);
            System.Threading.Thread.Sleep(125);
            Console.Beep(587, 125);
            Console.Beep(494, 125);
            System.Threading.Thread.Sleep(125);
            Console.Beep(523, 125);
            System.Threading.Thread.Sleep(250);
            Console.Beep(392, 125);
            System.Threading.Thread.Sleep(250);
            Console.Beep(330, 125);
            System.Threading.Thread.Sleep(250);
            Console.Beep(440, 125);
            System.Threading.Thread.Sleep(125);
            Console.Beep(494, 125);
            System.Threading.Thread.Sleep(125);
            Console.Beep(466, 125);
            System.Threading.Thread.Sleep(42);
            Console.Beep(440, 125);
            System.Threading.Thread.Sleep(125);
            Console.Beep(392, 125);
            System.Threading.Thread.Sleep(125);
            Console.Beep(659, 125);
            System.Threading.Thread.Sleep(125);
            Console.Beep(784, 125);
            System.Threading.Thread.Sleep(125);
            Console.Beep(880, 125);
            System.Threading.Thread.Sleep(125);
            Console.Beep(698, 125);
            Console.Beep(784, 125);
            System.Threading.Thread.Sleep(125);
            Console.Beep(659, 125);
            System.Threading.Thread.Sleep(125);
            Console.Beep(523, 125);
            System.Threading.Thread.Sleep(125);
            Console.Beep(587, 125);
            Console.Beep(494, 125);
            System.Threading.Thread.Sleep(375);
            Console.Beep(784, 125);
            Console.Beep(740, 125);
            Console.Beep(698, 125);
            System.Threading.Thread.Sleep(42);
            Console.Beep(622, 125);
            System.Threading.Thread.Sleep(125);
            Console.Beep(659, 125);
            System.Threading.Thread.Sleep(167);
            Console.Beep(415, 125);
            Console.Beep(440, 125);
            Console.Beep(523, 125);
            System.Threading.Thread.Sleep(125);
            Console.Beep(440, 125);
            Console.Beep(523, 125);
            Console.Beep(587, 125);
            System.Threading.Thread.Sleep(250);
            Console.Beep(784, 125);
            Console.Beep(740, 125);
            Console.Beep(698, 125);
            System.Threading.Thread.Sleep(42);
            Console.Beep(622, 125);
            System.Threading.Thread.Sleep(125);
            Console.Beep(659, 125);
            System.Threading.Thread.Sleep(167);
            Console.Beep(698, 125);
            System.Threading.Thread.Sleep(125);
            Console.Beep(698, 125);
            Console.Beep(698, 125);
            System.Threading.Thread.Sleep(625);
            Console.Beep(784, 125);
            Console.Beep(740, 125);
            Console.Beep(698, 125);
            System.Threading.Thread.Sleep(42);
            Console.Beep(622, 125);
            System.Threading.Thread.Sleep(125);
            Console.Beep(659, 125);
            System.Threading.Thread.Sleep(167);
            Console.Beep(415, 125);
            Console.Beep(440, 125);
            Console.Beep(523, 125);
            System.Threading.Thread.Sleep(125);
            Console.Beep(440, 125);
            Console.Beep(523, 125);
            Console.Beep(587, 125);
            System.Threading.Thread.Sleep(250);
            Console.Beep(622, 125);
            System.Threading.Thread.Sleep(250);
            Console.Beep(587, 125);
            System.Threading.Thread.Sleep(250);
            Console.Beep(523, 125);
            System.Threading.Thread.Sleep(1125);
            Console.Beep(784, 125);
            Console.Beep(740, 125);
            Console.Beep(698, 125);
            System.Threading.Thread.Sleep(42);
            Console.Beep(622, 125);
            System.Threading.Thread.Sleep(125);
            Console.Beep(659, 125);
            System.Threading.Thread.Sleep(167);
            Console.Beep(415, 125);
            Console.Beep(440, 125);
            Console.Beep(523, 125);
            System.Threading.Thread.Sleep(125);
            Console.Beep(440, 125);
            Console.Beep(523, 125);
            Console.Beep(587, 125);
            System.Threading.Thread.Sleep(250);
            Console.Beep(784, 125);
            Console.Beep(740, 125);
            Console.Beep(698, 125);
            System.Threading.Thread.Sleep(42);
            Console.Beep(622, 125);
            System.Threading.Thread.Sleep(125);
            Console.Beep(659, 125);
            System.Threading.Thread.Sleep(167);
            Console.Beep(698, 125);
            System.Threading.Thread.Sleep(125);
            Console.Beep(698, 125);
            Console.Beep(698, 125);
            System.Threading.Thread.Sleep(625);
            Console.Beep(784, 125);
            Console.Beep(740, 125);
            Console.Beep(698, 125);
            System.Threading.Thread.Sleep(42);
            Console.Beep(622, 125);
            System.Threading.Thread.Sleep(125);
            Console.Beep(659, 125);
            System.Threading.Thread.Sleep(167);
            Console.Beep(415, 125);
            Console.Beep(440, 125);
            Console.Beep(523, 125);
            System.Threading.Thread.Sleep(125);
            Console.Beep(440, 125);
            Console.Beep(523, 125);
            Console.Beep(587, 125);
            System.Threading.Thread.Sleep(250);
            Console.Beep(622, 125);
            System.Threading.Thread.Sleep(250);
            Console.Beep(587, 125);
            System.Threading.Thread.Sleep(250);
            Console.Beep(523, 125);
            System.Threading.Thread.Sleep(625);
        }
    }
}
